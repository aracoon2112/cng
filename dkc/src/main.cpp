// Auto generated code by esphome
// ========== AUTO GENERATED INCLUDE BLOCK BEGIN ===========
#include "esphome.h"
using namespace esphome;
logger::Logger *logger_logger;
wifi::WiFiComponent *wifi_wificomponent;
ota::OTAComponent *ota_otacomponent;
api::APIServer *api_apiserver;
using namespace sensor;
uart::UARTComponent *UART2;
sensor::Sensor *sensor_sensor;
sensor::Sensor *sensor_sensor_2;
sensor::Sensor *sensor_sensor_3;
sensor::Sensor *sensor_sensor_4;
sensor::Sensor *sensor_sensor_5;
sensor::Sensor *sensor_sensor_6;
sensor::Sensor *sensor_sensor_7;
sensor::Sensor *sensor_sensor_8;
#include "my_custom_component.h"
// ========== AUTO GENERATED INCLUDE BLOCK END ==========="

void setup() {
  // ===== DO NOT EDIT ANYTHING BELOW THIS LINE =====
  // ========== AUTO GENERATED CODE BEGIN ===========
  // esphome:
  //   name: dkc
  //   platform: ESP32
  //   board: esp32doit-devkit-v1
  //   includes:
  //   - my_custom_component.h
  //   libraries: []
  //   platformio_options: {}
  //   build_path: dkc
  //   arduino_version: espressif32@1.6.0
  App.pre_setup("dkc", __DATE__ ", " __TIME__);
  // logger:
  //   id: logger_logger
  //   baud_rate: 115200
  //   tx_buffer_size: 512
  //   logs: {}
  //   level: DEBUG
  //   hardware_uart: UART0
  logger_logger = new logger::Logger(115200, 512, logger::UART_SELECTION_UART0);
  logger_logger->pre_setup();
  App.register_component(logger_logger);
  // wifi:
  //   power_save_mode: NONE
  //   id: wifi_wificomponent
  //   reboot_timeout: 5min
  //   domain: .local
  //   fast_connect: false
  //   networks:
  //   - ssid: Pyralink Lab
  //     password: '25102017'
  //     id: wifi_wifiap
  //   use_address: dkc.local
  wifi_wificomponent = new wifi::WiFiComponent();
  wifi_wificomponent->set_use_address("dkc.local");
  wifi::WiFiAP wifi_wifiap = wifi::WiFiAP();
  wifi_wifiap.set_ssid("Pyralink Lab");
  wifi_wifiap.set_password("25102017");
  wifi_wificomponent->add_sta(wifi_wifiap);
  wifi_wificomponent->set_reboot_timeout(300000);
  wifi_wificomponent->set_power_save_mode(wifi::WIFI_POWER_SAVE_NONE);
  wifi_wificomponent->set_fast_connect(false);
  App.register_component(wifi_wificomponent);
  // ota:
  //   safe_mode: true
  //   id: ota_otacomponent
  //   port: 3232
  //   password: ''
  ota_otacomponent = new ota::OTAComponent();
  ota_otacomponent->set_port(3232);
  ota_otacomponent->set_auth_password("");
  ota_otacomponent->start_safe_mode();
  App.register_component(ota_otacomponent);
  // api:
  //   id: api_apiserver
  //   reboot_timeout: 5min
  //   port: 6053
  //   password: ''
  api_apiserver = new api::APIServer();
  App.register_component(api_apiserver);
  // sensor:
  api_apiserver->set_port(6053);
  api_apiserver->set_password("");
  api_apiserver->set_reboot_timeout(300000);
  // uart:
  //   id: UART2
  //   tx_pin: 17
  //   rx_pin: 16
  //   baud_rate: 115200
  UART2 = new uart::UARTComponent();
  App.register_component(UART2);
  // sensor.custom:
  //   platform: custom
  //   lambda: !lambda |-
  //     auto icar_sensor = new DKCComponent(id(UART2), 15000);
  //     App.register_component(icar_sensor);
  //     return {icar_sensor->I1,icar_sensor->U1,icar_sensor->S1,icar_sensor->W1,icar_sensor->I2,icar_sensor->U2,icar_sensor->S2,icar_sensor->W2};
  //     //return {icar_sensor->I1,icar_sensor->U1,icar_sensor->I2,icar_sensor->U2};
  //   sensors:
  //   - name: I relay 1
  //     unit_of_measurement: A
  //     accuracy_decimals: 2
  //     id: sensor_sensor
  //   - name: U relay 1
  //     unit_of_measurement: V
  //     accuracy_decimals: 2
  //     id: sensor_sensor_2
  //   - name: Status relay 1
  //     unit_of_measurement: 'on'
  //     accuracy_decimals: 0
  //     id: sensor_sensor_3
  //   - name: warning relay 1
  //     unit_of_measurement: 'on'
  //     accuracy_decimals: 0
  //     id: sensor_sensor_4
  //   - name: I relay 2
  //     unit_of_measurement: A
  //     accuracy_decimals: 2
  //     id: sensor_sensor_5
  //   - name: U relay 2
  //     unit_of_measurement: V
  //     accuracy_decimals: 2
  //     id: sensor_sensor_6
  //   - name: Status relay 2
  //     unit_of_measurement: 'on'
  //     accuracy_decimals: 0
  //     id: sensor_sensor_7
  //   - name: warning relay 2
  //     unit_of_measurement: 'on'
  //     accuracy_decimals: 0
  //     id: sensor_sensor_8
  //   id: custom_customsensorconstructor
  UART2->set_baud_rate(115200);
  UART2->set_tx_pin(17);
  UART2->set_rx_pin(16);
  custom::CustomSensorConstructor custom_customsensorconstructor = custom::CustomSensorConstructor([=]() -> std::vector<sensor::Sensor *> {
      auto icar_sensor = new DKCComponent(UART2, 15000);
      App.register_component(icar_sensor);
      return {icar_sensor->I1,icar_sensor->U1,icar_sensor->S1,icar_sensor->W1,icar_sensor->I2,icar_sensor->U2,icar_sensor->S2,icar_sensor->W2};
      //return {icar_sensor->I1,icar_sensor->U1,icar_sensor->I2,icar_sensor->U2};
  });
  sensor_sensor = custom_customsensorconstructor.get_sensor(0);
  App.register_sensor(sensor_sensor);
  sensor_sensor->set_name("I relay 1");
  sensor_sensor->set_unit_of_measurement("A");
  sensor_sensor->set_accuracy_decimals(2);
  sensor_sensor_2 = custom_customsensorconstructor.get_sensor(1);
  App.register_sensor(sensor_sensor_2);
  sensor_sensor_2->set_name("U relay 1");
  sensor_sensor_2->set_unit_of_measurement("V");
  sensor_sensor_2->set_accuracy_decimals(2);
  sensor_sensor_3 = custom_customsensorconstructor.get_sensor(2);
  App.register_sensor(sensor_sensor_3);
  sensor_sensor_3->set_name("Status relay 1");
  sensor_sensor_3->set_unit_of_measurement("on");
  sensor_sensor_3->set_accuracy_decimals(0);
  sensor_sensor_4 = custom_customsensorconstructor.get_sensor(3);
  App.register_sensor(sensor_sensor_4);
  sensor_sensor_4->set_name("warning relay 1");
  sensor_sensor_4->set_unit_of_measurement("on");
  sensor_sensor_4->set_accuracy_decimals(0);
  sensor_sensor_5 = custom_customsensorconstructor.get_sensor(4);
  App.register_sensor(sensor_sensor_5);
  sensor_sensor_5->set_name("I relay 2");
  sensor_sensor_5->set_unit_of_measurement("A");
  sensor_sensor_5->set_accuracy_decimals(2);
  sensor_sensor_6 = custom_customsensorconstructor.get_sensor(5);
  App.register_sensor(sensor_sensor_6);
  sensor_sensor_6->set_name("U relay 2");
  sensor_sensor_6->set_unit_of_measurement("V");
  sensor_sensor_6->set_accuracy_decimals(2);
  sensor_sensor_7 = custom_customsensorconstructor.get_sensor(6);
  App.register_sensor(sensor_sensor_7);
  sensor_sensor_7->set_name("Status relay 2");
  sensor_sensor_7->set_unit_of_measurement("on");
  sensor_sensor_7->set_accuracy_decimals(0);
  sensor_sensor_8 = custom_customsensorconstructor.get_sensor(7);
  App.register_sensor(sensor_sensor_8);
  sensor_sensor_8->set_name("warning relay 2");
  sensor_sensor_8->set_unit_of_measurement("on");
  sensor_sensor_8->set_accuracy_decimals(0);
  // =========== AUTO GENERATED CODE END ============
  // ========= YOU CAN EDIT AFTER THIS LINE =========
  App.setup();
}

void loop() {
  App.loop();
}
