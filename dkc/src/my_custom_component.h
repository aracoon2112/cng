#include "esphome.h"
#include <cstring>

#define REQ_HEADER 0x25
#define RES_HEADER 0x10

using namespace esphome;
using namespace esphome::uart;

typedef struct  relays{
  uint8_t ID ;
  uint8_t status;
  double I ;
  double U;
  bool warning ; 
} relay ; 

typedef struct Gps{
  unsigned long int time ;
  double longtitude ;
  double latitude ;
} gps;
class DKCComponent: public UARTDevice, public PollingComponent {
 public:
  DKCComponent(UARTComponent *parent, uint32_t update_interval ) : UARTDevice(parent), PollingComponent(update_interval)
  {

  }
  sensor::Sensor *I1 = new sensor::Sensor();
  sensor::Sensor *U1 = new sensor::Sensor();
  sensor::Sensor *W1 = new sensor::Sensor();
  sensor:: Sensor *S1 =new sensor :: Sensor();
  sensor :: Sensor *I2= new sensor:: Sensor() ;
  sensor :: Sensor *U2 = new sensor :: Sensor();
  sensor :: Sensor *W2 = new sensor :: Sensor();
  sensor :: Sensor *S2 = new sensor ::Sensor();
   //gps gpsInfor;
  relay* list;
  void send_request(uint8_t addr, uint8_t cmd){
    //uint8_t* buffer = new uint8_t[5];
    uint8_t buffer[5];
    buffer[0]=REQ_HEADER;
    buffer[1]= addr ;
    buffer[2]= 2;
    buffer[3]= cmd;
    buffer[4]= (~(buffer[0]+buffer[1]+buffer[2] + buffer[3])+1);
    write(buffer[0]);
    write(buffer[1]);
     write(buffer[2]);
    write(buffer[3]);
    write(buffer[4]);
  }


  void setup() override {
    // This will be called by App.setup()
    setTimeout(100);
  }

  void update() override {
    // This will be called every "update_interval" milliseconds.
    // request relay status 
    send_request(0xFF,0x20);
    // get response 
    while (read()!=0x10);
    if (read() == 0xA0){
      uint8_t len = read();
      if (read() == 0x00){
        uint8_t relayCount= read();
        list = new relay[(len-2)/7];
        uint8_t U, Um ;
        uint8_t I , Im ;
        uint16_t sum=0xA0+0x10+relayCount+len;
          for (uint8_t i =0 ; i < 2;i++){
              
              list[i].ID = read();
              list[i].status= read();
              U= read() ;
              Um =read(); 
              list[i].U= (double(U) +double(Um)/100);
              I=read();
              Im=read();
              list[i].I= (double(I)+double(Im)/100);
              //uint8_t cc = read() ;
              list[i].warning = read() ;
              sum+=list[i].ID+list[i].status+U +Um +I+Im +list[i].warning;
          }
          uint8_t cc =read();
          uint8_t checksum= ~sum+0x01;
          if (checksum == cc ) {
            I1->publish_state(list[0].I);
            U1->publish_state(list[0].U);
            W1->publish_state(list[0].warning);
            S1->publish_state(list[0].status);
            I2->publish_state(list[1].I);
            U2->publish_state(list[1].U);
            W2->publish_state(list[1].warning);
            S2->publish_state(list[1].status);
          }
        delete[] list;
      }
    }
    //get gps
 
}
};