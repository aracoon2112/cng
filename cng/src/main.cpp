// Auto generated code by esphome
// ========== AUTO GENERATED INCLUDE BLOCK BEGIN ===========
#include "esphome.h"
using namespace esphome;
using namespace text_sensor;
logger::Logger *logger_logger;
wifi::WiFiComponent *wifi_wificomponent;
ota::OTAComponent *ota_otacomponent;
api::APIServer *api_apiserver;
using namespace sensor;
sensor::Sensor *sensor_sensor;
text_sensor::TextSensor *text_sensor_textsensor;
sensor::Sensor *sensor_sensor_2;
text_sensor::TextSensor *text_sensor_textsensor_2;
sensor::Sensor *sensor_sensor_3;
text_sensor::TextSensor *text_sensor_textsensor_3;
sensor::Sensor *sensor_sensor_4;
text_sensor::TextSensor *text_sensor_textsensor_4;
sensor::Sensor *sensor_sensor_5;
sensor::Sensor *sensor_sensor_6;
#include "my_custom_component.h"
// ========== AUTO GENERATED INCLUDE BLOCK END ==========="

void setup() {
  // ===== DO NOT EDIT ANYTHING BELOW THIS LINE =====
  // ========== AUTO GENERATED CODE BEGIN ===========
  // esphome:
  //   name: cng
  //   platform: ESP32
  //   board: esp32doit-devkit-v1
  //   includes:
  //   - my_custom_component.h
  //   libraries:
  //   - WifiManager
  //   arduino_version: espressif32@1.6.0
  //   build_path: cng
  //   platformio_options: {}
  App.pre_setup("cng", __DATE__ ", " __TIME__);
  // text_sensor:
  // logger:
  //   level: NONE
  //   hardware_uart: UART0
  //   baud_rate: 115200
  //   logs: {}
  //   id: logger_logger
  //   tx_buffer_size: 512
  logger_logger = new logger::Logger(115200, 512, logger::UART_SELECTION_UART0);
  logger_logger->pre_setup();
  App.register_component(logger_logger);
  // wifi:
  //   domain: .local
  //   reboot_timeout: 5min
  //   fast_connect: false
  //   power_save_mode: NONE
  //   id: wifi_wificomponent
  //   networks:
  //   - ssid: Pyralink Lab
  //     password: '20171025'
  //     id: wifi_wifiap
  //   use_address: cng.local
  wifi_wificomponent = new wifi::WiFiComponent();
  wifi_wificomponent->set_use_address("cng.local");
  wifi::WiFiAP wifi_wifiap = wifi::WiFiAP();
  wifi_wifiap.set_ssid("Pyralink Lab");
  wifi_wifiap.set_password("20171025");
  wifi_wificomponent->add_sta(wifi_wifiap);
  wifi_wificomponent->set_reboot_timeout(300000);
  wifi_wificomponent->set_power_save_mode(wifi::WIFI_POWER_SAVE_NONE);
  wifi_wificomponent->set_fast_connect(false);
  App.register_component(wifi_wificomponent);
  // ota:
  //   password: ''
  //   safe_mode: true
  //   id: ota_otacomponent
  //   port: 3232
  ota_otacomponent = new ota::OTAComponent();
  ota_otacomponent->set_port(3232);
  ota_otacomponent->set_auth_password("");
  ota_otacomponent->start_safe_mode();
  App.register_component(ota_otacomponent);
  // api:
  //   password: ''
  //   reboot_timeout: 5min
  //   id: api_apiserver
  //   port: 6053
  api_apiserver = new api::APIServer();
  App.register_component(api_apiserver);
  // sensor:
  api_apiserver->set_port(6053);
  api_apiserver->set_password("");
  api_apiserver->set_reboot_timeout(300000);
  // sensor.custom:
  //   platform: custom
  //   lambda: !lambda |-
  //     auto icar_sensor = new DKCComponent( 15000);
  //     App.register_component(icar_sensor);
  //     return {icar_sensor->I1,icar_sensor->U1,icar_sensor->P1,icar_sensor->I2,icar_sensor->U2,icar_sensor->P2};
  //   sensors:
  //   - name: I of car's battery
  //     unit_of_measurement: A
  //     accuracy_decimals: 2
  //     id: sensor_sensor
  //   - name: U of car's battery
  //     unit_of_measurement: V
  //     accuracy_decimals: 2
  //     id: sensor_sensor_2
  //   - name: Power of car's battery
  //     unit_of_measurement: W
  //     accuracy_decimals: 2
  //     id: sensor_sensor_3
  //   - name: I of backup battery
  //     unit_of_measurement: A
  //     accuracy_decimals: 2
  //     id: sensor_sensor_4
  //   - name: U of backup battery
  //     unit_of_measurement: V
  //     accuracy_decimals: 2
  //     id: sensor_sensor_5
  //   - name: Power of backup battery
  //     unit_of_measurement: W
  //     accuracy_decimals: 2
  //     id: sensor_sensor_6
  //   id: custom_customsensorconstructor
  // text_sensor.custom:
  //   platform: custom
  //   lambda: !lambda |-
  //     auto icar_sensor = new DKCComponent( 15000);
  //     App.register_component(icar_sensor);
  //     return {icar_sensor->S1,icar_sensor->W1,icar_sensor->S2,icar_sensor->W2};
  //   text_sensors:
  //   - name: Status of car's battery
  //     id: text_sensor_textsensor
  //   - name: Warning of car's battery
  //     id: text_sensor_textsensor_2
  //   - name: Status of backup battery
  //     id: text_sensor_textsensor_3
  //   - name: Warning of backup battery
  //     id: text_sensor_textsensor_4
  //   id: custom_customtextsensorconstructor
  custom::CustomSensorConstructor custom_customsensorconstructor = custom::CustomSensorConstructor([=]() -> std::vector<sensor::Sensor *> {
      auto icar_sensor = new DKCComponent( 15000);
      App.register_component(icar_sensor);
      return {icar_sensor->I1,icar_sensor->U1,icar_sensor->P1,icar_sensor->I2,icar_sensor->U2,icar_sensor->P2};
  });
  sensor_sensor = custom_customsensorconstructor.get_sensor(0);
  App.register_sensor(sensor_sensor);
  sensor_sensor->set_name("I of car's battery");
  sensor_sensor->set_unit_of_measurement("A");
  sensor_sensor->set_accuracy_decimals(2);
  custom::CustomTextSensorConstructor custom_customtextsensorconstructor = custom::CustomTextSensorConstructor([=]() -> std::vector<text_sensor::TextSensor *> {
      auto icar_sensor = new DKCComponent( 15000);
      App.register_component(icar_sensor);
      return {icar_sensor->S1,icar_sensor->W1,icar_sensor->S2,icar_sensor->W2};
  });
  text_sensor_textsensor = custom_customtextsensorconstructor.get_text_sensor(0);
  App.register_text_sensor(text_sensor_textsensor);
  text_sensor_textsensor->set_name("Status of car's battery");
  sensor_sensor_2 = custom_customsensorconstructor.get_sensor(1);
  App.register_sensor(sensor_sensor_2);
  sensor_sensor_2->set_name("U of car's battery");
  sensor_sensor_2->set_unit_of_measurement("V");
  sensor_sensor_2->set_accuracy_decimals(2);
  text_sensor_textsensor_2 = custom_customtextsensorconstructor.get_text_sensor(1);
  App.register_text_sensor(text_sensor_textsensor_2);
  text_sensor_textsensor_2->set_name("Warning of car's battery");
  sensor_sensor_3 = custom_customsensorconstructor.get_sensor(2);
  App.register_sensor(sensor_sensor_3);
  sensor_sensor_3->set_name("Power of car's battery");
  sensor_sensor_3->set_unit_of_measurement("W");
  sensor_sensor_3->set_accuracy_decimals(2);
  text_sensor_textsensor_3 = custom_customtextsensorconstructor.get_text_sensor(2);
  App.register_text_sensor(text_sensor_textsensor_3);
  text_sensor_textsensor_3->set_name("Status of backup battery");
  sensor_sensor_4 = custom_customsensorconstructor.get_sensor(3);
  App.register_sensor(sensor_sensor_4);
  sensor_sensor_4->set_name("I of backup battery");
  sensor_sensor_4->set_unit_of_measurement("A");
  sensor_sensor_4->set_accuracy_decimals(2);
  text_sensor_textsensor_4 = custom_customtextsensorconstructor.get_text_sensor(3);
  App.register_text_sensor(text_sensor_textsensor_4);
  text_sensor_textsensor_4->set_name("Warning of backup battery");
  sensor_sensor_5 = custom_customsensorconstructor.get_sensor(4);
  App.register_sensor(sensor_sensor_5);
  sensor_sensor_5->set_name("U of backup battery");
  sensor_sensor_5->set_unit_of_measurement("V");
  sensor_sensor_5->set_accuracy_decimals(2);
  sensor_sensor_6 = custom_customsensorconstructor.get_sensor(5);
  App.register_sensor(sensor_sensor_6);
  sensor_sensor_6->set_name("Power of backup battery");
  sensor_sensor_6->set_unit_of_measurement("W");
  sensor_sensor_6->set_accuracy_decimals(2);
  // =========== AUTO GENERATED CODE END ============
  // ========= YOU CAN EDIT AFTER THIS LINE =========
  App.setup();
}

void loop() {
  App.loop();
}
