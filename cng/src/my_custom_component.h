#include "esphome.h"
#include <cstring>
#include <WiFiManager.h>

#define REQ_HEADER 0x25
#define RES_HEADER 0x10
#define TOUT 500
#define DEV 1
using namespace esphome;
//using namespace esphome::uart;

typedef struct  relays{
  uint8_t ID ;
  const char* status;
  double I ;
  double U;
  const char* warning ; 
} relay ; 

typedef struct Gps{
  unsigned long int time ;
  double longtitude ;
  double latitude ;
} gps;
class DKCComponent:  public PollingComponent {
 public:
  DKCComponent( uint32_t update_interval ) :  PollingComponent(update_interval)
  {

  }
//WiFiManager wifiManager;

  sensor::Sensor *I1 = new sensor::Sensor();
  sensor::Sensor *U1 = new sensor::Sensor();
  text_sensor::TextSensor *W1 = new text_sensor::TextSensor();
  text_sensor:: TextSensor *S1 =new text_sensor :: TextSensor();
  sensor :: Sensor *I2= new sensor:: Sensor() ;
  sensor :: Sensor *U2 = new sensor :: Sensor();
  text_sensor :: TextSensor *W2 = new text_sensor :: TextSensor();
  text_sensor :: TextSensor *S2 = new text_sensor ::TextSensor();
  sensor :: Sensor *P1 = new sensor :: Sensor();
  sensor :: Sensor *P2 = new sensor :: Sensor() ;
   //gps gpsInfor;
  relay* list;
  void send_request(uint8_t addr, uint8_t cmd){
    //uint8_t* buffer = new uint8_t[5];
    uint8_t buffer[5];
    buffer[0]=REQ_HEADER;
    buffer[1]= addr ;
    buffer[2]= 2;
    buffer[3]= cmd;
    buffer[4]= (~(buffer[0]+buffer[1]+buffer[2] + buffer[3])+1);
    Serial2.write(buffer,5);
  }
  bool check_packet(uint8_t* packet ,uint8_t len){
      if (packet[0]!=0x10) return false;
      uint16_t sum =0;
      for (uint8_t i =0 ; i < len-1 ; i++){
        sum +=(uint16_t) packet[i];
      } 
      sum = ~sum +1;
      uint8_t cc = (uint8_t) sum ;
      if (packet[len-1]!= cc) return false;
      return true ;
  }
  bool get_response(){
    unsigned long timeOld = millis();
    unsigned long timeNew= millis();
    while(!Serial2.available()){
      timeNew = millis();
      if ((unsigned long)  (timeNew-timeOld)>TOUT ){ 
        #if DEV
          Serial.println("uart time out");
        #endif
        return false ;
      }
    }
    uint8_t resLen = Serial2.available();
    uint8_t packet[resLen]={};
    Serial2.readBytes(packet, resLen);
    if (!(check_packet(packet,resLen))) {
      #if DEV
        Serial.println("check packet : false");
      #endif
      return false;
    }
    #if DEV 
      Serial.print("receive response from uart 2 : ");
      for (int i =0 ; i < resLen -1 ;i++) {
        Serial.print(packet[i],HEX);
        Serial.print(", ");
      }
      Serial.println(packet[resLen-1],HEX);
    #endif
    list = new relay[packet[4]];
    for (uint8_t i =0 ; i<2;i++){
      if (packet[5+7*i]==0x50){
      list[0].ID = packet[5+7*i];
      list[0].status= packet[6+7*i]?"on":"off";
      list[0].U= (double) packet[7+7*i]+(double) packet[8+7*i]/100;
      list[0].I = (double) packet[9+7*i] + (double) packet[10+7*i]/100;
      list[0].warning = packet[11+7*i]?"yes":"no" ;
      } else {
      list[1].ID = packet[5+7*i];
      list[1].status= packet[6+7*i]?"on":"off";
      list[1].U= (double) packet[7+7*i]+(double) packet[8+7*i]/100;
      list[1].I = (double) packet[9+7*i] + (double) packet[10+7*i]/100;
      list[1].warning = packet[11+7*i]?"yes":"no" ;
      }
    }
    return true;
  }
  void setup() override {
    // This will be called by App.setup()
   // wifiManager.autoConnect("AutoConnectAP");
   Serial2.begin(115200);
   Serial.begin(115200);
   // setTimeout(100);
  }

  void update() override {
    // This will be called every "update_interval" milliseconds.
    // request relay status 
    send_request(0xFF,0x20);
    // get response 
      bool flag= get_response();
      if (flag){
            I1->publish_state(list[0].I);
            U1->publish_state(list[0].U);
            W1->publish_state(list[0].warning);
            S1->publish_state(list[0].status);
            I2->publish_state(list[1].I);
            U2->publish_state(list[1].U);
            W2->publish_state(list[1].warning);
            S2->publish_state(list[1].status);
            P1->publish_state(list[0].I*list[0].U);
            P2->publish_state(list[1].I*list[1].U);
            delete[] list;
          }
        

    //get gps
 
}
};